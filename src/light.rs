use super::*;

pub struct DirectionalLight{
    pub direction : Vector3<f64>,
    pub color : LinSrgba,
    pub strength : f64
}

impl DirectionalLight{
    pub fn new(direction : Vector3<f64>, color : LinSrgba, strength : f64) -> Self{
        DirectionalLight{
            direction : direction.normalize(),
            color,
            strength
        }
    }
}