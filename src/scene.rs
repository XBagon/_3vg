use super::*;

pub struct Scene{
    pub objects : Vec<Box<Object>>,
    pub lights : Vec<DirectionalLight>,
    pub background_color : LinSrgba
}

impl Scene{
    pub fn new(objects : Vec<Box<Object>>, lights : Vec<DirectionalLight>, background_color : LinSrgba) -> Self{
        Scene{
            objects,
            lights,
            background_color
        }
    }
}