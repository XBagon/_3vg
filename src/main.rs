#![allow(unused_imports)]

extern crate image;
extern crate cgmath;
extern crate palette;

mod primitives;
mod renderer;
mod scene;
mod light;

use cgmath::{Vector3,Vector2,Point3,InnerSpace,EuclideanSpace,MetricSpace,Deg};
use renderer::Renderer;
use scene::Scene;
use primitives::Object;
use primitives::plane::Plane;
use primitives::sphere::Sphere;
use primitives::line::Line;
use light::DirectionalLight;
use image::{Pixel,ColorType,Rgb,Rgba,Luma,LumaA};
use palette::{Pixel as PalettePixel,LinSrgba,Blend,Shade,Alpha,Color};

fn main() {
    let renderer = Renderer::new(
        Point3::new(0.0,0.0,0.0),
        Plane::new(
            Point3::new(0.0,0.0,10.0),
            Vector3::new(0.0,0.0,0.0),
            Vector3::new(1.0,0.0,0.0),
            Vector3::new(0.0,1.0,0.0),
            19.20,
            10.80
        ),
        Vector2::new(1920,1080)
    );
    renderer.render(&Scene::new(
        vec![
            Box::new(Sphere::new(Point3::new(-2.0,3.0,20.0),5.0, LinSrgba::new(0.1,0.1,0.1,1.0))),
            Box::new(Sphere::new(Point3::new(7.0,1.0,15.0),2.0, LinSrgba::new(0.13,0.65,0.28,1.0))),
            Box::new(Sphere::new(Point3::new(0.0,-4.0,40.0),10.0, LinSrgba::new(0.87,0.26,0.08,1.0))),
            Box::new(Sphere::new(Point3::new(-6.5,5.0,13.0),1.0, LinSrgba::new(0.76,0.00,0.95,1.0))),
            Box::new(Sphere::new(Point3::new(-7.0,4.0,13.0),1.0, LinSrgba::new(0.31,0.78,0.31,1.0)))
        ],
        vec![
            DirectionalLight::new(Vector3::new(0.0,-1.0,0.0),LinSrgba::new(1.0,1.0,1.0,1.0),1.0),
            //DirectionalLight::new(Vector3::new(0.0,1.0,0.0),LinSrgba::new(1.0,1.0,1.0,1.0),0.8),
            //DirectionalLight::new(Vector3::new(0.0,1.0,0.0),LinSrgba::new(194,2,242,255),1.0)
        ],
        LinSrgba::new(0.3,0.7,0.7,1.0)
    ));
}

