use super::*;

pub struct Sphere{
    pub position : Point3<f64>,
    pub radius : f64,
    pub color : LinSrgba
}

impl Sphere{
    pub fn new(position : Point3<f64>, radius : f64, color : LinSrgba) -> Self{
        Sphere{
            position,
            radius,
            color
        }
    }
}

impl Object for Sphere{
    fn ray_cast(&self, ray : &Line) -> Vec<(Point3<f64>,Vector3<f64>)> {
        let mut result = vec![];
        let det : f64 = (ray.direction.dot(ray.origin - self.position)).powf(2f64)-(ray.origin - self.position).magnitude().powf(2f64)+self.radius.powf(2f64);
        if det >= 0f64 {
            let d0 = -ray.direction.dot(ray.origin - self.position) - det.sqrt() - 1e-10; //1e-10 floating point correction
            if d0.is_sign_positive() {
                let p0 = ray.origin + ray.direction * d0;
                result.push((p0, p0 - self.position));
            }
            if det==0f64 {return result;}
            let d1 = -ray.direction.dot(ray.origin - self.position) + det.sqrt() - 1e-10; //1e-10 floating point correction
            if d1.is_sign_positive() {
                let p1 = ray.origin + ray.direction * d1;
                result.push((p1, p1 - self.position));
            }

//            if ray.origin.distance2(d0) < ray.origin.distance2(d1) {
//                return Some((d0,d0-self.position));
//            }else{
//                return Some((d1,d1-self.position));
//            }
        }
        result
    }

    fn color(&self) -> LinSrgba {
        self.color
    }
}