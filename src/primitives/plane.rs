use super::*;

pub struct Plane {
    pub position : Point3<f64>,
    pub normal : Vector3<f64>,
    pub right : Vector3<f64>,
    pub up : Vector3<f64>,
    pub width : f64,
    pub height : f64
}

impl Plane{
    pub fn new(position : Point3<f64>, normal : Vector3<f64>, right : Vector3<f64>, up : Vector3<f64>, width : f64, height : f64) -> Self{
        Plane{
            position,
            normal,
            right,
            up,
            width,
            height,
        }
    }
}