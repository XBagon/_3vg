use super::*;

pub struct Line{
    pub origin : Point3<f64>,
    pub direction : Vector3<f64>
}

impl Line{
    pub fn new(origin : Point3<f64>, direction : Vector3<f64>) -> Self{
        Line{
            origin,
            direction: direction.normalize(),
        }
    }

    pub fn from_points(origin : Point3<f64>, point : Point3<f64>) -> Self{
        Line{
            origin,
            direction: (point-origin).normalize(),
        }
    }
}