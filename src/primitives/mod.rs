pub mod sphere;
pub mod plane;
pub mod line;

use super::*;

pub trait Object{
    fn ray_cast(&self, ray : &Line)  -> Vec<(Point3<f64>,Vector3<f64>)> ;
    fn color(&self) -> LinSrgba;
}