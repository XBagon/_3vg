use super::*;

pub struct Renderer{
    position : Point3<f64>,
    viewport : Plane,
    resolution : Vector2<u32>
}

impl Renderer{
    pub fn new(position : Point3<f64>, viewport : Plane, resolution : Vector2<u32>) -> Self{
        Renderer{
            position,
            viewport,
            resolution,
        }
    }

    pub fn render(&self, scene : &Scene){
        let mut imgbuf = image::RgbaImage::new(self.resolution.x, self.resolution.y);
        for (x, y, pixel) in imgbuf.enumerate_pixels_mut() {
            let render_ray = Line::from_points(self.position, self.viewport.position + self.viewport.right * (x as f64/self.resolution.x as f64 - 0.5) * self.viewport.width + self.viewport.up * (0.5 - y as f64/self.resolution.y as f64) * self.viewport.height);
            let hits = Self::ray_cast_objects(&render_ray, scene);
            let mut color;
            if hits.len() > 0{
                color = Self::determine_color(&hits,scene);
            }else{
                color = scene.background_color;
            }

            *pixel = Rgba(color.into_format().into_raw());
        }
        imgbuf.save("test.png").unwrap();
    }

    ///returns every hit with any object in the scene
    fn ray_cast_objects<'a>(ray : &Line, scene : &'a Scene) -> Vec<(&'a Box<Object>,(Point3<f64>, Vector3<f64>))>{
        let mut hits  = vec![];
        for obj in scene.objects.iter() {
            let current_hits = obj.ray_cast(&ray);
            for current_hit in current_hits {
                hits.push((obj,current_hit));
            }
        }
        hits.sort_by(|(_o0,h0),(_o1,h1)|{ray.origin.distance2(h1.0).partial_cmp(&ray.origin.distance2(h0.0)).unwrap()});
        hits
    }

    fn determine_color<'a>(hits : &Vec<(&'a Box<Object>,(Point3<f64>, Vector3<f64>))>, scene : &Scene) -> LinSrgba{
        let mut color = LinSrgba::new(0.0,0.0,0.0,1.0);
        for hit in hits.iter() {
            let (obj,(_point,normal)) = hit;
            let mut hit_color= LinSrgba::new(0.0,0.0,0.0,1.0);
            for light in scene.lights.iter() {
                let light_eff = ((normal.dot(-light.direction)*light.strength).max(0.0)) as f32;
                let lit_color = LinSrgba{ color : obj.color().color * light.color.color * light_eff, alpha :  obj.color().alpha * light.color.alpha };
                hit_color = hit_color.screen(lit_color); // * 1 / (distance * distance)
                if Self::hit_by_shadow(&light, &hit, scene) {
                    hit_color= LinSrgba::new(0.0,0.0,0.0,1.0);
                }
            }
            color = hit_color.over(color);
        }
        color
    }

    fn hit_by_shadow<'a>(light: &DirectionalLight, hit : &(&'a Box<Object>,(Point3<f64>, Vector3<f64>)), scene : &Scene) -> bool{
        Self::ray_cast_objects(&Line::new((hit.1).0,-light.direction),scene).len() == 2
    }
}